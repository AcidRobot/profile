export PS1="\n\[\033[1;36m\]\u @\\H\[\033[1;37m\] \[\033[0;36m\]`date`\n\[\033[0m\][\[\033[1;31m\]\w\[\033[0m\]]"

#Aliases
alias install="sudo apt-get install"

#Misc Aliases
#
alias ls="ls -G"
alias ll="ls -l -G -S"
alias numfiles="echo $(ls -1 | wc -l)"          #Count the number of files in current directory
. ~/.bash_aliases

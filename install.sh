#!/bin/bash
pwd
sudo apt install i3 i3blocks git pavucontrol ldap-utils -y
cp -va ".i3blocks.conf" ~/.i3blocks.conf
mkdir -p ~/.i3
cp -av "i3.config" ~/.i3/config
cp -av ".bash_profile" ~/.bash_profile
cp -av ".gitconfig" ~/.gitconfig
mkdir -p ~/Pictures/wallpapers && cp -av "starwars.png" $_
